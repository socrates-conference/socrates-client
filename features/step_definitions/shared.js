/* eslint-disable babel/no-invalid-this */
import {defineSupportCode} from 'cucumber';
import App from '../../src/App';
import * as React from 'react';
import {mount} from 'enzyme';
import {select} from '../support/cssSelectors';

defineSupportCode(function ({Given, When}) {
  Given('the application is running', function () {
    this.wrapper = mount(<App store={this.sagaTester.store} history={this.history}/>,
      {attachTo: this.root});
  });

  When('select the checkbox {string}', function (checkboxId) {
    click(checkboxId);
  });
});

function click(selector) {
  select(selector).click();
}
