// @flow
export type Config = {
  environment: string,
  jwtSecret: string,
  siteUrl: string,
  siteAttendeeUrl: string,
  serverBackend: string,
  isRegistrationOpen: boolean
}
