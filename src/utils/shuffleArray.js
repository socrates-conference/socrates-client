// @flow

export default function shuffleArray(array: Array<Object>): Array<Object> {
  if(!array || array.length <= 1) {
    return array;
  }
  let currentIndex = array.length;
  let temporaryValue: Object;
  let randomIndex: number;

  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

