// @flow
import 'raf/polyfill';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

if(!global.catchAll) {
  process.on('unhandledRejection', function (err) {
    console.error('Unhandled promise rejection: %o', err);
  });
  global.catchAll = true;
}

Enzyme.configure({adapter: new Adapter()});

// hack to make modules using exenv (such as react-modal) render correctly.
global.window = {document: {createElement: () => {}}, addEventListener: () => {}};

// hack to enable instana tracing
global.ineum = () => {};

const localStorageMock = {
  setItem: () => {},
  getItem: () => {},
  removeItem: () => {}
};
global.localStorage = localStorageMock;
