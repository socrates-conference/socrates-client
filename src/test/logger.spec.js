import createLogger from './logger';

describe('logger should', () => {
  let consoleLog;
  let debugMode;

  beforeEach(() => {
    consoleLog = console.log;
    debugMode = process.env.DEBUG;
  });

  afterEach(() => {
    console.log = consoleLog;
    process.env.DEBUG = debugMode;
  });

  const store = {
    getState: () => ({})
  };

  const action = {
    type: 'SOMETHING'
  };

  const logger = createLogger();

  const nextMiddleware = (dispatchedAction) => {
    expect(dispatchedAction.type).toEqual('SOMETHING');
    return {};
  };

  it('write to console.log in debug mode', done => {
    process.env.DEBUG = true;
    console.log = () => {
      done();
    };

    logger(store)(nextMiddleware)(action);
  });

  it('not write to console.log in non-debug mode', () => {
    process.env.DEBUG = false;
    console.log = () => {
      fail();
    };

    logger(store)(nextMiddleware)(action);
  });
});