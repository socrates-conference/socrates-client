// @flow

import AuthenticationEvent from '../events/authenticationEvents';
import * as jwt from 'jsonwebtoken';
import config from '../../config';
import {getToken} from '../services/jwtStorage';

export type AuthenticationState = {
  token: string,
  isAdministrator: boolean,
  userName: string,
  hasFinished: boolean
}

const INITIAL_STATE: AuthenticationState = {
  token: '',
  userName: 'Guest',
  isAdministrator: false,
  hasFinished: false
};

const initialState = (initial: AuthenticationState): AuthenticationState => {
  const token = getToken();
  if (!token) {
    return initial;
  }

  const {
    name: userName,
    isAdministrator,
    email
  } = jwt.verify(token, config.jwtSecret);

  return {
    ...initial,
    token,
    userName,
    isAdministrator,
    email
  };
};

const authenticationReducer = (state: AuthenticationState = initialState(INITIAL_STATE), action: any) => {
  switch (action.type) {
    case AuthenticationEvent.LOGIN_SUCCESS:
      return {
        ...state,
        hasFinished: true,
        token: action.token,
        isAdministrator: action.data.isAdministrator,
        userName: action.data.name
      };
    case AuthenticationEvent.LOGIN_STARTED:
    case AuthenticationEvent.LOGOUT_SUCCESS:
      return {...state, isAdministrator: false, hasFinished: false, token: '', userName: 'Guest'};
    case AuthenticationEvent.LOGIN_ERROR:
      return {...state, isAdministrator: false, hasFinished: true, token: '', userName: 'Guest'};
    default:
      return state;
  }
};

export default authenticationReducer;
