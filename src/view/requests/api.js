// @flow

import type {RoomTypeOptions} from '../application/form/RoomTypeOptions';
import client from './client';
import config from '../../config';
import {getToken} from '../services/jwtStorage';
import {setAuthorizationHeader} from './clientConfig';

const BACKEND_URL = '/server/api/v1';

const addInterestedPerson = (name: string, email: string): Promise<boolean> =>
  client.post(`${BACKEND_URL}/interested-people`, {name, email});

const confirmInterestedPerson = (consentKey: string): Promise<boolean> =>
  client.post(`${BACKEND_URL}/interested-people/confirm`, {consentKey});

const removeInterestedPerson = (email: string): Promise<boolean> =>
  client.delete(`${BACKEND_URL}/interested-people/${email}`);

export type ApplicationData = {
  diversity: string,
  email: string,
  nickname: string,
  roomTypes: Array<string>
};

type ApplicationResponse = {
    personId: number,
    nickname: string,
    email: string,
    roomTypes: Array<string>,
    diversityReason: string,
}

const addApplication = (params: ApplicationData ): Promise<ApplicationResponse> =>
  client.post(`${BACKEND_URL}/applications`, params);

const getRoomTypeOptions = (): Promise<RoomTypeOptions> =>
  client.get(`${BACKEND_URL}/applications/options`).then(x => x.data);

if (process.env.NODE_ENV !== 'test') { // TODO
  const token = getToken();
  if (token) {
    setAuthorizationHeader(token);
  }
}

const login = (email: string, password: string): Promise<string> => {
  return client.post(`${config.siteUrl}${config.serverBackend}/login`, {email, password})
    .then(response => response.data.token);
};

export default {addInterestedPerson, confirmInterestedPerson, removeInterestedPerson, login,
  addApplication, getRoomTypeOptions};
