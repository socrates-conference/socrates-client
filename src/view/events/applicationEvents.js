// @flow

import type {RoomTypeOptions} from '../application/form/RoomTypeOptions';
import {messageTypes} from '../application/form/applicationMessages';

export type ApplicationEvent = {
  type: string,
  messageType?: string,
  roomTypeOptions?: RoomTypeOptions
}

export default class ApplicationEvents {
  static get ADD_SUCCEEDED() { return 'ApplicationEvent/ADD_SUCCEEDED'; }

  static get ADD_FAILED() { return 'ApplicationEvent/ADD_FAILED'; }

  static get ROOM_TYPES_RECEIVED() { return 'ApplicationEvent/ROOM_TYPES_RECEIVED'; }
}

export const roomTypesReceived =
  (roomTypeOptions: RoomTypeOptions) => ({type: ApplicationEvents.ROOM_TYPES_RECEIVED, roomTypeOptions});
export const addSucceeded = () => ({type: ApplicationEvents.ADD_SUCCEEDED, messageType: messageTypes.SUCCESS});
export const addFailed = (messageType: string) => ({type: ApplicationEvents.ADD_FAILED, messageType});