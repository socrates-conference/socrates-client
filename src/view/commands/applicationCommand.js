// @flow

export default class ApplicationCommand {
  static get ADD_APPLICATION () { return 'ApplicationCommand/ADD_APPLICATION'; }
  static get FETCH_ROOM_TYPES () { return 'ApplicationCommand/FETCH_ROOM_TYPES'; }
}

export const addApplication = (nickname: string, email: string, roomTypes: string[], diversity: string) => ({
  type: ApplicationCommand.ADD_APPLICATION,
  nickname,
  email,
  roomTypes,
  diversity
});
export const fetchRoomTypes = () => ({type: ApplicationCommand.FETCH_ROOM_TYPES});