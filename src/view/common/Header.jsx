// @flow

import React from 'react';
import './Header.css';
import logo from '../../assets/img/socrates_logo_2020.png';

export default function Header() {
  return <div id="header" className="jumbotron">
    <header className="container socrates-header">
      <div className="row">
        <div className="col-lg-4 col-sm-12"><img id="logo" src={logo} alt="SoCraTes 2020"/></div>
        <div className="col-lg-8 col-sm-12">
          <h1 className="socrates-title">SoCraTes 2020</h1>
          <h2>10th International Conference for Software Craft and Testing</h2>
          <h2>August 27 - 30, 2020 • Soltau, Germany</h2>
          <hr/>
          <a className="btn btn-primary pull-right btn-application"
            href="https://scrts.de/socrates2019/index.html">See the 2019 schedule!</a>
        </div>
      </div>
    </header>
  </div>;
}
