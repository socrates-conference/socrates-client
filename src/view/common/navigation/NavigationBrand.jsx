// @flow

import {Link} from 'react-router-dom';
import React from 'react';
import brandLogo from '../../../assets/img/socrates_no_text_40.png';

export default function NavigationBrand() {
  return (
    <Link className="navbar-brand" to="/home" title="Home">
      <span>
        <img src={brandLogo} alt="logo"/>
      </span>
      <span> SoCraTes 2020</span>
    </Link>
  );
}
