// @flow
import {NavLink} from 'react-router-dom';
import React from 'react';
import './Navigation.css';

type NavigationLinkProps = {
  url: string,
  title: string,
  iconClass: string
};

export default function NavigationLink(props: NavigationLinkProps) {
  const iconCss = 'navLinkIcon ' + props.iconClass;
  return (
    <li className="nav-item">
      <NavLink
        className="nav-link"
        to={props.url}
        title={props.title}
        data-toggle="collapse"
        data-target=".navbar-collapse.show"
      >
        <span className={iconCss} />
        <span className="navLinkText d-md-none d-lg-none d-xl-inline"> {props.title}</span>
      </NavLink>
    </li>
  );
}
