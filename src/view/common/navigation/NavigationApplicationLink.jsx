// @flow
import React from 'react';

type Props = {visible: boolean};
export default function NavigationApplicationLink({visible}: Props) {
  return (
    <li className={'nav-item' + (visible ? ' d-block' : ' d-none')}>
      <a title="2019 Schedule" className="btn btn-primary pull-right btn-application"
        href="https://scrts.de/socrates2019/index.html">2019 schedule</a>
    </li>
  );
}

