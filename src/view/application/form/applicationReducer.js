// @flow

import ApplicationEvents from '../../events/applicationEvents';
import type {ApplicationEvent} from '../../events/applicationEvents';
import type {RoomTypeOptions} from './RoomTypeOptions';
import {messages} from './applicationMessages';

type State = {
  message: string,
  messageType: string,
  roomTypeOptions: RoomTypeOptions
};

const INITIAL_STATE = {
  message: '',
  messageType: '',
  roomTypeOptions: []
};

const applicationReducer = (state: State = INITIAL_STATE, event: ApplicationEvent ) => {
  switch(event.type) {
    case ApplicationEvents.ROOM_TYPES_RECEIVED:
      return {...state, roomTypeOptions: event.roomTypeOptions};
    case ApplicationEvents.ADD_SUCCEEDED:
    case ApplicationEvents.ADD_FAILED:
      return {...state, message: messages[event.messageType], messageType: event.messageType};
    default:
      return state;
  }
};

export default applicationReducer;