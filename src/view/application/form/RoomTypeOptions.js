// @flow

export type RoomTypeOption = {
  value: string, label: string, prices: Array<string>
}

type Price = {| amount: number, display: string |};
export type RoomTypeOptions = Array<{| value: string, label: string, prices: Array<Price> |}>
