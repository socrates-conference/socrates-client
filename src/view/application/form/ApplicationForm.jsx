// @flow

import React from 'react';
import PropTypes from 'prop-types';
import {RoomTypeInput} from './RoomTypeInput';
import {NicknameInput} from './NicknameInput';
import {EmailInput} from './EmailInput';
import {DiversityInput} from './DiversityInput';
import type {RoomTypeOptions} from './RoomTypeOptions';
import config from '../../../config';
import ExternalLink from '../../common/ExternalLink';

type Handlers = {
  onNicknameChange: SyntheticInputEvent<HTMLInputElement> => void,
  onEmailChange: SyntheticInputEvent<HTMLInputElement> => void,
  onSubmit: SyntheticInputEvent<HTMLInputElement> => void,
  onRoomTypeClick: SyntheticInputEvent<HTMLInputElement> => void,
  onDiversityChange: string => void
}

type Values = {
  nickname: string,
  email: string,
  roomTypeSelected: Array<string>,
  diversitySelected: string
}

type ApplicationFormProps = {
  handlers: Handlers,
  hasValidNickname: boolean,
  hasValidEmail: boolean,
  isEnabled: boolean,
  roomTypeOptions: RoomTypeOptions,
  values: Values,
  isDataPrivacyConfirmedChecked: boolean,
  dataPrivacyConfirmedChange: () => void

}

export function ApplicationForm(props: ApplicationFormProps) {
  const validationClassName = isValid => isValid ? 'is-valid' : 'is-invalid';
  const checkboxValidationClassName = (isValid) => `form-control ${isValid ? 'is-valid' : 'is-invalid'}`;
  const validNickname = validationClassName(props.hasValidNickname);
  const validEmail = validationClassName(props.hasValidEmail);
  const {email, diversitySelected, nickname, roomTypeSelected} = props.values;
  const {onEmailChange, onNicknameChange, onRoomTypeClick, onDiversityChange, onSubmit} = props.handlers;
  const dataPrivacyConfirmedClass = checkboxValidationClassName(props.isDataPrivacyConfirmedChecked);

  return (
    <form className="form container" id="application-form" onSubmit={onSubmit}>
      <div className="form-row">
        <h4 className="withMargin">
          Apply for the lottery
        </h4>
      </div>
      <div className="form-row">
        <NicknameInput validationClass={validNickname} value={nickname} onChange={onNicknameChange}/>
        <EmailInput validationClass={validEmail} value={email} onChange={onEmailChange}/>
      </div>
      <div className="form-row">
        <RoomTypeInput
          options={props.roomTypeOptions}
          selected={roomTypeSelected}
          onClick={onRoomTypeClick}
        />
      </div>
      <div className="form-row">
        <DiversityInput
          diversitySelected={diversitySelected}
          onDiversityChange={onDiversityChange}
        />
      </div>
      <div className="form-row">
        <p className="col-form-label">You cannot register twice, please make sure your data is correct.</p>
      </div>
      <div className="col-12 mb-2">
        <div className="input-group">
          <div className="input-group-prepend">
            <div className="input-group-text">
              <input
                id="application-dataPrivacy"
                type="checkbox" aria-labelledby="application-privacy-label" value="dataPrivacyConfirmed"
                checked={props.isDataPrivacyConfirmedChecked} onChange={props.dataPrivacyConfirmedChange}
              />
            </div>
          </div>
          <label id="application-privacy-label" className={dataPrivacyConfirmedClass}>
            <small>
              <div>
                I agree that my details from the application form will be collected and processed to determine the
                conference participants. The data will remain stored until the current year conference is over.
              </div>
              <div>
                Note: You can revoke your consent at any time for the future via e-mail to
                registration AT socrates MINUS conference DOT de, thereby we will delete your application
                / registration.
              </div>
              <div>
                Detailed information on handling user data can be found in our <ExternalLink
                  url={`${config.siteUrl}/privacy-policy`} target="_blank"
                  title="privacy policy"> data privacy policy</ExternalLink>
              </div>
            </small>
          </label>
        </div>
      </div>

      <div className="form-row">
        <button
          className="btn btn-primary"
          disabled={!props.isEnabled}
          id="submit-application"
          type="submit"
        >
          I really do want to participate!
        </button>
      </div>
    </form>
  );
}

ApplicationForm.propTypes = {
  dataPrivacyConfirmedChange: PropTypes.func.isRequired,
  handlers: PropTypes.object.isRequired,
  hasValidEmail: PropTypes.bool.isRequired,
  hasValidNickname: PropTypes.bool.isRequired,
  isDataPrivacyConfirmedChecked: PropTypes.bool.isRequired,
  isEnabled: PropTypes.bool.isRequired,
  roomTypeOptions: PropTypes.array.isRequired,
  values: PropTypes.object.isRequired
};
