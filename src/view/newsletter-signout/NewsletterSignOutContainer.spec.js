import setUpJsDom from '../../test/setupJsDom';
import {mount, shallow} from 'enzyme';
import React from 'react';
import {REMOVE_SUCCEEDED} from './NewsletterSignOutConstants';
import {NewsletterSignOutContainer} from './NewsletterSignOutContainer';
import {MemoryRouter, Route} from 'react-router';

describe('NewsletterSignOutContainer', () => {
  let newsletterSignOut;
  let root;

  function setInputValue(selector, value) {
    const wrapper = newsletterSignOut.find(selector);
    const node = wrapper.instance();
    node.value = value;
    wrapper.simulate('change', {target: {value}});
    newsletterSignOut.update();
  }

  function fillForm(email) {
    setInputValue('#newsletter-sign-out-email', email);
  }

  function fillFormAndClickButton(email) {
    fillForm(email);
    newsletterSignOut.find('#newsletter-sign-out-form-button').simulate('click');
  }

  it('should render without exploding', () => {
    newsletterSignOut = shallow(<NewsletterSignOutContainer message=""/>, {attachTo: root});
    expect(newsletterSignOut).toHaveLength(1);
  });

  it('should render correctly', () => {
    newsletterSignOut = shallow(<NewsletterSignOutContainer message=""/>, {attachTo: root});
    expect(newsletterSignOut.instance().state.message).toEqual('');
    expect(newsletterSignOut).toMatchSnapshot();
  });

  describe('when receiving props', () => {
    it('should render with params', () => {
      const match = {params: {data: 'test@test.de'}};
      newsletterSignOut =
        shallow(<NewsletterSignOutContainer match={match} message=""/>, {attachTo: root});
      expect(newsletterSignOut).toMatchSnapshot();
    });

    it('should render with message', () => {
      newsletterSignOut =
        shallow(<NewsletterSignOutContainer message={REMOVE_SUCCEEDED}/>, {attachTo: root});
      expect(newsletterSignOut).toMatchSnapshot();
    });

  });

  describe('when filling in email form field', () => {
    let mockSignOut;
    beforeEach(() => {
      root = setUpJsDom();
      mockSignOut = jest.fn();
      newsletterSignOut = mount(
        <MemoryRouter initialEntries={['/newsletter-sign-out/alex@email.de']}>
          <Route path="/newsletter-sign-out/:data?">
            <NewsletterSignOutContainer message="" signOut={mockSignOut}/>
          </Route>
        </MemoryRouter>, {attachTo: root});
    });

    it('should be valid for well-formatted email', () => {
      fillForm('valid@example.com');
      expect(newsletterSignOut.find('#newsletter-sign-out-email').instance().className).toContain('is-valid');
    });

    it('should be invalid for poorly formatted email', () => {
      fillForm('invalidExample.com');
      expect(newsletterSignOut.find('#newsletter-sign-out-email').instance().className).toContain('is-invalid');
    });

    it('should be invalid for empty field', () => {
      fillForm('');
      expect(newsletterSignOut.find('#newsletter-sign-out-email').instance().className).toContain('is-invalid');
    });

    it('should send signUp command when form is submitted', () => {
      fillFormAndClickButton('valid@example.com');
      expect(mockSignOut).toHaveBeenCalledTimes(1);
    });
  });

});