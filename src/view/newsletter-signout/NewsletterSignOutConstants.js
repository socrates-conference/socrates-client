// @flow

export const REMOVE_SUCCEEDED: string = 'We\'re sorry you\'re leaving. ' +
  'You have successfully signed out of the SoCraTes Germany newsletter.';
export const REMOVE_FAILED: string = 'We currently cannot remove you from the list. Please try again later.';
export const EMPTY: string = '';
