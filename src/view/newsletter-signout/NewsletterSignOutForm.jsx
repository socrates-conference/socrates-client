// @flow

import React from 'react';
import './newsletterSignOut.css';

export type Props = {
  email: string,
  hasValidEmail: boolean,
  message: string,
  onEmailChange: (value: string) => void,
  onSubmit: () => void,
  isDisabled: boolean
}

export default function NewsletterForm(props: Props) {

  const klass = (isValid) => `form-control mb-2 ${isValid ? 'is-valid' : 'is-invalid'}`;
  const emailClass = klass(props.hasValidEmail);

  const onEmailChange = (event: SyntheticEvent<HTMLInputElement>) => {
    props.onEmailChange(event.currentTarget.value);
  };

  const onSubmit = (event: Event) => {
    event.preventDefault();
    props.onSubmit();
  };

  const renderMessage = () => {
    if (props.message && props.message !== '') {
      return (<div id="newsletter-message" className="pulse">{props.message}</div>);
    }
    return '';
  };

  return (<div>
    <div className="segment-header">
      <h1>Newsletter Sign Out</h1>
    </div>
    <div>
      <p>At this point you can unsubscribe from the SoCraTes newsletter.</p>
      <p>If that is what you want to do, just enter your email address and click the <i>unsubscribe</ i> button.
        From this point on, no more newsletters will be sent to the provided e-mail address.</p>
    </div>
    <form className="form">
      <div className="row">
        <div className="col-lg-1 col-md-2 col-xs-3 align-self-center">
          <label htmlFor="email">Email: </label>
        </div>
        <div className="col-lg-11 col-md-10 col-xs-9">
          <input
            id="newsletter-sign-out-email" name="email" type="email" className={emailClass}
            placeholder="Your email" required onChange={onEmailChange}
            value={props.email}/>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <button
            id="newsletter-sign-out-form-button" className="btn btn-primary mb-2"
            disabled={props.isDisabled}
            onClick={onSubmit}>Unsubscribe
          </button>
        </div>
      </div>
    </form>
    {renderMessage()}
  </div>);
}
