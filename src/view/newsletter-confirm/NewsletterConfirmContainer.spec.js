import {shallow} from 'enzyme';
import React from 'react';
import {NewsletterConfirmContainer} from './NewsletterConfirmContainer';
import {CONFIRM_SUCCEEDED} from './NewsletterConfirmConstants';

describe('NewsletterConfirmContainer', () => {
  let newsletterConfirm;
  let root;

  it('should render without exploding', () => {
    newsletterConfirm = shallow(<NewsletterConfirmContainer message=""/>, {attachTo: root});
    expect(newsletterConfirm).toHaveLength(1);
  });

  it('should render correctly', () => {
    newsletterConfirm = shallow(<NewsletterConfirmContainer message=""/>, {attachTo: root});
    expect(newsletterConfirm).toMatchSnapshot();
  });

  describe('when receiving props', () => {
    it('should render with params', () => {
      const match = {params: {data: 'test@example.com'}};
      newsletterConfirm =
        shallow(<NewsletterConfirmContainer match={match} message=""/>, {attachTo: root});
      expect(newsletterConfirm).toMatchSnapshot();
    });

    it('should render with message', () => {
      newsletterConfirm =
        shallow(<NewsletterConfirmContainer message={CONFIRM_SUCCEEDED}/>, {attachTo: root});
      expect(newsletterConfirm).toMatchSnapshot();
    });

  });

});