// @flow
/* eslint-disable */
import type {Config} from '../../src/ConfigType';

const config: Config = {
  environment: 'dev',
  jwtSecret: '%jwt_secret%',
  siteUrl: 'http://18.197.143.144',
  siteAttendeeUrl: 'http://18.197.143.144/attendee',
  serverBackend: '/server/api/v1',
  isRegistrationOpen: true
};
export default config;
