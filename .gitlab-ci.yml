stages:
  - install
  - analyze
  - test
  - coverage
  - build
  - deploy
  - report

cache:
  paths:
   - node_modules/
   - yarn.lock

install:
  stage: install
  image: node:11
  script:
    - yarn
    - mkdir coverage
    - mkdir .nyc_output
  artifacts:
    paths:
    - node_modules
    - coverage
    - .nyc_output
    expire_in: 1 hour

analyze:lint:
  stage: analyze
  image: node:11
  dependencies:
    - install
  script:
    - yarn build-css
    - yarn lint
  only:
    - develop

analyze:react-lint:
  stage: analyze
  image: node:11
  dependencies:
    - install
  script:
    - yarn build-css
    - yarn react-scripts-build
  only:
    - develop

analyze:flow:
  stage: analyze
  image: node:11
  dependencies:
    - install
  script:
    - yarn build-css
    - yarn flow
  only:
    - develop

test:unit:
  stage: test
  image: node:11
  dependencies:
    - install
  script:
    - yarn test:coverage:unit
  artifacts:
    paths:
      - node_modules
      - coverage
      - .nyc_output
    expire_in: 1 hour

coverage:collect:
  stage: coverage
  image: node:8
  dependencies:
    - test:unit
  script:
    - yarn test:coverage:collect
  artifacts:
    paths:
      - coverage
    expire_in: 1 day

dev:build:
  stage: build
  image: node:11
  script:
    - sed -e 's/%jwt_secret%/'"$JWT_SECRET_DEV"'/g' rancher/dev/config.js > src/config.js
    - yarn
    - yarn build
  artifacts:
    paths:
      - build
    expire_in: 1 day
  only:
    - develop
  allow_failure: false

dev:deploy:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  dependencies:
    - dev:build
  script:
    - aws s3 sync ./build/ s3://dev.socrates-conference.de --acl public-read --delete
  only:
    - develop
  allow_failure: false

prod:build:
  stage: build
  image: node:11
  script:
    - sed -e 's/%jwt_secret%/'"$JWT_SECRET_PROD"'/g' rancher/prod/config.js > src/config.js
    - yarn
    - yarn build
  artifacts:
    paths:
      - build
    expire_in: 1 day
  only:
    - master
  allow_failure: false

prod:deploy:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  dependencies:
    - prod:build
  script:
    - aws s3 sync ./build/ s3://socrates-conference.de --acl public-read --delete
  only:
    - master
  allow_failure: false

pages:
  stage: report
  dependencies:
    - coverage:collect
  script:
    - mv coverage/* public
    - mv public/gitlab_page.html public/index.html
  artifacts:
    paths:
      - public/
    expire_in: 30 days
  only:
    - develop
  when: always
